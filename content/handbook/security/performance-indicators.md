---
title: Security Department Performance Indicators
---

{{% alert title="Warning" color="warning" %}}
This page is deprecated and will move soon to the internal handbook.

See https://gitlab.com/gitlab-com/gl-security/security-department-meta/-/issues/1685
(internal issue).
{{% /alert %}}

{{< performance-indicators "security_department" >}}
