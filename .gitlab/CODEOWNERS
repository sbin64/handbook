#####
# Quick intro to Codeowners
#
# The formmat is:
# [section]
# /<directory> <gitlab-user>
#
# The file is organized in to named sections denoting an internal organization function. e.g. People Group, Engineering, etc.
#
# ^[section] - Denotes an optional set of CodeOwners
#              No ^ means one of these Codeowners are required to approve MRs for this section
#
# For more information see https://docs.gitlab.com/ee/user/project/codeowners/
#
# Special notes:  Due to the current implementation in our code base sections can't have spaces in their names.
#                 Sections can't have named codeowners e.g. [section] @codeowner
#
#####

# The following entries are all sections which require codeowner approval
# either because they relate to the function of the Handbook or because
# they are a controlled Document.

# No point having a controlled documents section if anyone can just update
# The CODEOWNERS file.
[CODEOWNERS]
.gitlab/CODEOWNERS @david @timzallmann @gitlab-com/ceo-chief-of-staff-team

# The following entries relate to the operation of the handbook repo or the handbook site itself.
[handbook-operations]
/.gitignore @jamiemaynard @marshall007
/.gitlab-ci.yml @jamiemaynard @marshall007
/.markdownlint-cli2.jsonc @jamiemaynard @marshall007
/.markdownlint.yaml @jamiemaynard @marshall007
/go.sum @jamiemaynard @marshall007
/go.mod @jamiemaynard @marshall007
/package-lock.json @jamiemaynard @marshall007
/package.json @jamiemaynard @marshall007
/.gitlab/ @gitlab-com/ceo-chief-of-staff-team
/config/ @jamiemaynard @marshall007
/scripts/ @jamiemaynard @marshall007
/static/ @jamiemaynard @marshall007
/content/handbook/about/ @jamiemaynard
/content/handbook/content-websites/ @jamiemaynard
/content/preferences/ @jamiemaynard

[handbook-homepage]
/content/_index.html @jamiemaynard @marshall007
/content/featured-background.png @jamiemaynard @marshall007
/layouts/home.html @jamiemaynard @marshall007

[redirects]
/layouts/index.redirects @jamiemaynard @marshall007

[ide-configs]
/.editorconfig @jamiemaynard @marshall007
/.nova/ @jamiemaynard @marshall007
/.vscode/ @jamiemaynard @marshall007
/.idea/ @jamiemaynard @marshall007

[docs]
/LICENCE @jamiemaynard @marshall007
/README.md @jamiemaynard @marshall007
/content/docs/ @jamiemaynard @marshall007

# This is the controlled documents section where approval from a CODEOWNER is always required
# Entries will be disabled if they form part of a migration.
[Controlled Documents]
/content/handbook/people-group/acceptable-use-policy/ @wendybarnes @akramer @emilyplotkin @pegan
/content/handbook/security/access-management-policy.md @joshlemos @jlongo_gitlab
/content/handbook/security/security-assurance/security-compliance/access-reviews.md @jburrows001 @tdilbeck @corey-oas @lcoleman @jlongo_gitlab
/content/handbook/security/security-engineering/application-security/vulnerability-management.md @joshlemos @jritchey
/content/handbook/security/audit-logging-policy.md @joshlemos @jlongo_gitlab
/content/handbook/engineering/infrastructure/production/index.md.erb @marin @alanrichards
/content/handbook/business-technology/gitlab-business-continuity-plan/index.md @brobins @cmestel @NabithaRao
/content/handbook/security/security-assurance/security-risk/storm-program/business-impact-analysis.md @jburrows001 @tdilbeck @corey-oas @lcoleman @jlongo_gitlab
/content/handbook/engineering/infrastructure/change-management/index.md @marin @alanrichards @meks @joergheilig
/content/handbook/security/controlled-document-procedure.md @joshlemos @jlongo_gitlab
/content/handbook/security/cryptographic-standard.md @joshlemos @jlongo_gitlab
/content/handbook/security/data-classification-standard.md @joshlemos @jlongo_gitlab
/content/handbook/legal/privacy/dpia-policy/ @robin
/content/handbook/business-technology/data-team/data-management/ @amiebright @dvanrooijen2 @iweeks @nmcavinue
/content/handbook/business-technology/data-team/platform/index.md @amiebright @dvanrooijen2 @iweeks
/content/handbook/engineering/infrastructure/database/disaster_recovery.md @marin @alanrichards
/content/handbook/security/threat-management/vulnerability-management/encryption-policy.md @smanzuik @estrike
/content/handbook/business-technology/end-user-services/onboarding-access-requests/endpoint-management/ @rrea1 @pkaldis @NabithaRao @ericrubin @mbeee
/content/handbook/security/security-assurance/security-compliance/security-control-lifecycle.md @jburrows001 @tdilbeck @corey-oas @lcoleman @jlongo_gitlab
/content/handbook/security/password-standard.md @joshlemos @jlongo_gitlab
/content/handbook/security/isms.md @joshlemos @jlongo_gitlab
/content/handbook/security/threat-management/vulnerability-management/infrastructure-vulnerability-procedure.md @smanzuik @estrike
/content/handbook/business-technology/end-user-services/self-help-troubleshooting/ @rrea1 @pkaldis @NabithaRao @ericrubin @mbeee
/content/handbook/business-technology/end-user-services/onboarding-access-requests/ @rrea1 @pkaldis @NabithaRao @ericrubin @mbeee
/content/handbook/engineering/infrastructure/network-security/ @marin @alanrichards
/content/handbook/people-group/offboarding/offboarding_standards/ @wendybarnes @mpatel8
/content/handbook/security/penetration-testing-policy.md @joshlemos @jlongo_gitlab
/content/handbook/people-policies/ @cgudgenov @emilyplotkin @pegan @wendybarnes
/content/handbook/engineering/infrastructure/production/architecture/ @marin @alanrichards
/content/handbook/security/records-retention-deletion.md @joshlemos @jlongo_gitlab
/content/handbook/security/security-assurance/security-risk/storm-program/ @jburrows001 @tdilbeck @corey-oas @lcoleman @jlongo_gitlab
/content/handbook/security/security-change-management-procedure.md @joshlemos @jlongo_gitlab
/content/handbook/security/security-assurance/observation-management-procedure.md @jburrows001 @tdilbeck @corey-oas @lcoleman @jlongo_gitlab
/content/handbook/security/security-assurance/observation-remediation-procedure.md @jburrows001 @tdilbeck @corey-oas @lcoleman @jlongo_gitlab
/content/handbook/security/security-operations/sirt/security-incident-communication-plan.md @joshlemos @jfuentes2
/content/handbook/security/security-operations/sirt/sec-incident-response.md @joshlemos @jfuentes2
/content/handbook/security/secops-oncall.md @joshlemos @jlongo_gitlab @jfuentes2
/content/handbook/security/security-assurance/governance/sec-training.md @jburrows001 @tdilbeck @corey-oas @lcoleman @jlongo_gitlab
/content/handbook/security/security-assurance/security-risk/third-party-risk-management.md @jburrows001 @tdilbeck @corey-oas @lcoleman @jlongo_gitlab
/content/handbook/security/token-management-standard.md @joshlemos @jlongo_gitlab
/content/handbook/security/threat-management/vulnerability-management/ @smanzuik @estrike
/content/handbook/security/change-management-policy.md @joshlemos @jlongo_gitlab
/content/handbook/finance/expenses/ @brobins
/content/handbook/security/security-assurance/dedicated-compliance/POAM-Deviation-Request-Procedure.md @jburrows001 @tdilbeck @corey-oas @lcoleman @jlongo_gitlab
/content/handbook/security/security-assurance/control-health-effectiveness-rating.md @jburrows001 @tdilbeck @corey-oas @lcoleman @jlongo_gitlab
/content/handbook/security/change-management-policy.md @joshlemos @jlongo_gitlab

# These optional entries are for when no codeowner is found.
^[content-sites]
* @jamiemaynard @marshall007

^[job-families]
/content/job-families/ @sytses @streas

^[handbook]
/content/handbook/ @sytses @streas

^[company]
/content/company/ @sytses @streas

# The rest of the entries are for the different sections of the handbook and/or job-families
^[READMEs]
/content/handbook/ceo/chief-of-staff-team/readmes/dlangemak.md @dlangemak
/content/handbook/ceo/chief-of-staff-team/readmes/ipedowitz.md @ipedowitz
/content/handbook/ceo/chief-of-staff-team/readmes/jamiemaynard.md @jamiemaynard
/content/handbook/ceo/chief-of-staff-team/readmes/lfarrer.md @lfarrer
/content/handbook/ceo/chief-of-staff-team/readmes/streas.md @streas

^[alliances]
/content/handbook/alliances/ @nbadiey
/content/job-families/alliances/ @nbadiey

^[ceo]
/content/job-families/chief-executive-officer @sytses @streas
/content/job-families/board-of-directors/ @sytses @robin
/sites/handbook/source/handbook/leadership/ @sytses @streas
/content/handbook/company/ @sytses @streas
/content/handbook/company/strategy/ @sytses
/content/handbook/company/stewardship/ @sytses
/content/handbook/company/okrs/ @streas
/content/handbook/company/history/ @sytses
/content/handbook/company/quote-to-cash/ @justinfarris @ofernandez2 @tgolubeva @courtmeddaugh @alex_martin @vincywilson @jackib @chloeliu @jeromezng @jameslopez @csouthard @rhardarson @isandin @shreyasagarwal @jbrennan1 @jrabbits @NabithaRao @sbaranidharan @jesssalcido @iweeks @lwhelihan @s_mccauley @annapiaseczna @andrew_murray @msubramanian

^[chief-information-security-officer]
/content/job-families/chief-information-security-officer.md @robin @fofungwu

^[comms]
/content/news/ @sliang2

^[company]
/content/handbook/esg.md @robin @slcline
/content/handbook/company/structure/ @wendybarnes @cbednarz @anjali_kaufmann @dparsonage @glucchesi @carlierussell @rtakken
/content/handbook/company/family-and-friends-day/ @chrisweber44

^[Working-Groups]
/content/handbook/company/working-groups/ @sytses @streas
/content/handbook/company/working-groups/clickhouse-datastore/ @clefelhocz1 @sgoldstein @nhxnguyen @nicolewilliams
/content/handbook/company/working-groups/cross-functional-prioritization/ @clefelhocz1 @vkarnes @meks @justinfarris
/content/handbook/company/working-groups/demo-test-data/ @grantyoung @poffey21 @vincywilson @meks
/content/handbook/company/working-groups/api-vision/ @g.hickman @arturoherrero @timzallmann
/content/handbook/company/working-groups/disaster-recovery/  @mjwood @andrashorvath
/content/handbook/company/working-groups/frontend-observability/ @timzallmann @samdbeckham
/content/handbook/company/working-groups/multi-large/ @cdu1 @mbursi
/content/handbook/company/working-groups/software-supply-chain-security/ @sam.white @pcalder
/content/handbook/company/working-groups/ai-integration/ @hbenson @timzallmann @tmccaslin @wayne

^[customer-success]
/content/handbook/customer-success @spatching @christiaanconover @mrleutz

^[eba-team]
/content/handbook/eba/ @chrisweber44

^[engineering]
/content/handbook/engineering/horse/pubsec/ @sdumesnil @pmartinsgl @craig
/content/handbook/engineering/horse/ @marin
/content/handbook/engineering/infrastructure @sloyd @marin
/content/handbook/engineering/fedramp-compliance @connorgilbert @corey-oas @Julia.Lake
/content/handbook/engineering/ @akramer
/content/job-families/engineering/ @joergheilig
/content/job-families/engineering/incubation/ @bmarnane

^[finance]
/content/handbook/finance/documents/ @james.shen @watson.lin
/content/handbook/finance/ @brobins
/content/job-families/finance/ @brobins

^[Infrastructure-Standards]
/content/handbook/infrastructure-standards/ @jeffersonmartin @dawsmith @alanrichards

^[legal]
/content/handbook/legal-and-compliance/ @rschulman
/content/job-families/legal-and-corporate-affairs/ @robin

^[marketing]
/content/handbook/marketing/marketing-strategy-and-platform/ @RLeihe268
/content/handbook/marketing/marketing-strategy-and-platform/growth/ @s_awezec
/content/handbook/marketing/marketing-strategy-and-platform/marketing-operations/ @christinelee
/content/handbook/marketing/ @akramer
/content/job-families/marketing/ @akramer


^[People]
/content/handbook/leadership/underperformance/ @wendybarnes @pegan @cbednarz @rtakken @glucchesi
/content/handbook/people-policies/ @cgudgenov @emilyplotkin @pegan @wendybarnes
/content/handbook/people-group/ @wendybarnes @pegan @cbednarz @rallen3 @mpatel8 @cgudgenov
/content/handbook/people-group/acceptable-use-policy/ @wendybarnes @akramer @emilyplotkin @pegan
/content/handbook/people-group/employment-solutions/ @pegan @hdevlin
/content/handbook/people-group/contracts-probation-periods/ @mpatel8 @shensiek @alex_venter @Mowry @nutaurus
/content/handbook/people-group/people-compliance/ @cgudgenov
/content/handbook/people-group/general-onboarding/ @mpatel8 @Mowry @alex_venter @anechan @ashjammers @nutaurus
/content/handbook/people-group/promotions-transfers/ @wendybarnes @mpatel8
/content/handbook/people-group/offboarding/offboarding_standards/ @mpatel8 @Mowry @alex_venter @anechan @ashjammers @nutaurus
/content/handbook/people-group/offboarding/ @wendybarnes @mpatel8
/content/handbook/people-group/relocation/ @wendybarnes @pegan @mpatel8
/content/handbook/people-group/employment-branding/ @drogozinski @cchiodo @rallen3
/content/handbook/people-group/employment-branding/people-communications/ @kaylagolden @drogozinski @rallen3
/content/handbook/group-conversations/ @chrisweber44 
/content/handbook/people-group/celebrations/ @wendybarnes @mpatel8
/content/handbook/people-group/talent-assessment/ @wendybarnes @rtakken @glucchesi @pegan @cbednarz
/content/handbook/labor-and-employment-notices/ @pegan @wendybarnes @cgudgenov @streas @sytses
/content/handbook/people-policies/leave-of-absence/ @lyndemeiers @pegan @cgudgenov
/content/handbook/people-policies/leave-of-absence/us/ @lyndemeiers @pegan @cgudgenov
/content/handbook/people-group/team-member-relations/ @pegan @atisdale
/content/handbook/people-group/pronouns/ @klang @ahanselka @lmcnally1
/content/job-families/people-group/recruiting-operations-insights/ @sytses @streas @wendybarnes @rallen3
/content/job-families/people-group/recruiter.md @sytses @streas @wendybarnes @rallen3
/content/job-families/people-group/recruiting-sourcer.md @sytses @streas @wendybarnes @rallen3
/content/job-families/people-group/candidate-experience.md @sytses @streas @wendybarnes @rallen3
/content/job-families/people-group/ @wendybarnes @pegan
/content/handbook/people-group/pronouns/ @klang @ahanselka @lmcnally1

^[People-Engineering]
/content/handbook/people-group/engineering/ @anechan

^[product]
/content/handbook/direction/ @david @fseifoddini @brhea
/content/handbook/product/ @david @fseifoddini @brhea
/content/handbook/product/efficiency/ @joshlambert
/content/handbook/product/saas-efficiency/ @ipedowitz @dsteer @s_awezec @joshlambert @gl-free-saas-user-efficiency-leaders
/data/performance_indicators/ @david @fseifoddini @brhea
/content/handbook/company/performance-indicators/product/ @david @fseifoddini @brhea
/data/performance_indicators/enablement_section.yml @joshlambert
/content/handbook/company/performance-indicators/product/ops-section/ @kbychu @jreporter
/data/performance_indicators/ops_section.yml @kbychu @jreporter
/data/performance_indicators/sec_section.yml @hbenson
/data/performance_indicators/dev_section.yml @ogolowinski
/data/performance_indicators/data_science_section.yml @hbenson @tmccaslin
/content/handbook/product/fulfillment/ @ofernandez2 @tgolubeva @courtmeddaugh @alex_martin @doniquesmit
/content/handbook/product/fulfillment/cloudlicensing/ @courtmeddaugh
/content/handbook/product/fulfillment/provision/ @courtmeddaugh
/content/handbook/handbook/product/fulfillment/licensingissues/ @courtmeddaugh
/content/handbook/product/fulfillment/self-service-purchase/ @alex_martin
/content/handbook/product/fulfillment/qsr/ @tgolubeva
/content/handbook/product/fulfillment/auto-renewal/ @tgolubeva
/content/handbook/company/performance-indicators/product/fulfillment-section/ @ofernandez2 @tgolubeva @courtmeddaugh @alex_martin @doniquesmit
/content/job-families/product/technical-writer.md @susantacker @dianalogan @kpaizee
/content/job-families/product/product-design-management.md @vkarnes
/content/job-families/product/product-designer.md @vkarnes
/content/job-families/product/ux-fullstack-engineer.md @susantacker
/content/job-families/product/ux-research-manager.md @asmolinski2
/content/job-families/product/ux-research-operations-coordinator.md @asmolinski2
/content/job-families/product/ux-researcher.md @asmolinski2
/content/job-families/product/technical-writing-manager.md @susantacker @dianalogan @kpaizee
/content/job-families/product/pricing.md @justinfarris
/content/job-families/product/product-analyst.md @justinfarris @cbraza
/content/job-families/product/product-management-leadership.md @david @hbenson @mflouton @justinfarris
/content/job-families/product/product-manager.md @david @justinfarris @hbenson @mflouton @joshlambert @fzimmer @jreporter @ofernandez2
/content/job-families/product/product-operations.md @justinfarris
/content/job-families/product/ @david @clenneville @mflouton @justinfarris @hbenson

^[sales]
/content/handbook/sales/ @dhong
/content/handbook/company/performance-indicators/sales.md @dhong
/data/performance_indicators/sales.yml @dhong
/content/job-families/sales/ @dhong @jbrennan1 @jakebielecki @james_harrison @jdbeaumont @cfarris

^[security]
/content/job-families/security/ @joshlemos @jlongo_gitlab
/content/handbook/security/ @joshlemos @jlongo_gitlab @plafoucriere
/content/handbook/security/architecture/ @plafoucriere
/content/handbook/security/security-operations/ @jfuentes2
/content/handbook/security/security-assurance/ @jburrows001 @tdilbeck @corey-oas @jlongo_gitlab @lcoleman
/content/handbook/security/threat-management/ @smanzuik @estrike
/content/handbook/security/threat-management/red-team/ @cmoberly
/content/handbook/security/security-engineering/ @jritchey
/content/handbook/security/security-engineering/automation/  @agroleau
/content/handbook/security/security-engineering/infrastructure-security/ @joe-dub
/content/handbook/security/security-engineering/security-logging/ @joe-dub

[support]
/content/handbook/support/ @gitlab-com/support
/content/handbook/support/workflows/how-to-set-up-shift-coverage-for-an-event.md @gitlab-com/support/managers
/content/handbook/support/workflows/personal_data_access_account_deletion.md @jcolyer @izzyfee @lyle
/content/handbook/support/workflows/account_deletion_access_request_workflows.md @jcolyer @izzyfee @lyle
/content/handbook/support/workflows/data_processing_addendums.md @lyle
/content/handbook/support/workflows/information-request.md @lasayers @dfrhodes @emccrann @smccreesh @BronwynBarnett @rachelpack
/content/handbook/support/workflows/log_requests.md @gitlab-com/support/managers
/content/handbook/support/workflows/pii_removal_requests.md @gitlab-com/support/managers @gitlab-com/gl-security/security-operations/trust-and-safety
/content/handbook/support/workflows/dmca.md @gitlab-com/support/managers @gitlab-com/gl-security/security-operations/trust-and-safety
/content/handbook/support/workflows/reinstating-blocked-accounts.md @gitlab-com/support/managers @gitlab-com/gl-security/security-operations/trust-and-safety
/content/handbook/support/workflows/working_with_security.md @gitlab-com/support/managers @gitlab-com/gl-security
/content/handbook/support/workflows/unbabel_translation_in_zendesk.md @gitlab-com/support/managers @gitlab-com/support/support-ops
/content/handbook/support/workflows/setting_ticket_priority.md @gitlab-com/support/managers @gitlab-com/support/support-ops
/content/handbook/support/workflows/zendesk-instances.md @gitlab-com/support/managers @gitlab-com/support/support-ops
/content/handbook/support/workflows/team/performance_review.md @joergheilig @lbot @lyle @shaunmccann @vparsons

[support-readiness]
/content/handbook/support/readiness/ @lyle @jcolyer

^[teampps]
/content/teamops/ @nshah19 @streas @cynthia
/content/handbook/teamops/ @streas @cynthia
/content/handbook/teamops/partners.md @jmoverley @streas @cynthia

^[total-rewards]
/content/handbook/total-rewards/ @brittanyr @mwilkins

[values]
/content/handbook/values/ @sytses @gitlab-com/ceo-chief-of-staff-team

^[working-groups]
/content/handbook/company/structure/working-groups/ @sytses @streas
/content/handbook/company/structure/working-groups/clickhouse-datastore.md @clefelhocz1 @sgoldstein @nhxnguyen @nicolewilliams
/content/handbook/company/structure/working-groups/cross-functional-prioritization.md @clefelhocz1 @vkarnes @meks @justinfarris
/content/handbook/company/structure/working-groups/demo-test-data.md @grantyoung @poffey21 @vincywilson @meks
/content/handbook/company/structure/working-groups/api-vision.md @g.hickman @arturoherrero @timzallmann
/content/handbook/company/structure/working-groups/disaster-recovery.md  @mjwood @andrashorvath
/content/handbook/company/structure/working-groups/frontend-observability.md @timzallmann @samdbeckham
/content/handbook/company/structure/working-groups/multi-large.md @cdu1 @mbursi
/content/handbook/company/structure/working-groups/software-supply-chain-security.md @sam.white @pcalder
/content/handbook/company/structure/working-groups/ai-integration.md @hbenson @timzallmann @tmccaslin @wayne
